import { StyleSheet } from 'react-native';

export const brand = {
  colorPrimary: "#577590",
  colorAccent: "#f3ca40",
  colorAlt: "#f2a541",
  colorPop: "#f08a4b",
  colorDark: "#d78a76",
  colorWhite: '#eee',
  fontLarge: 30,
  fontMedium: 20,
  fontSmall: 10,
  flexLarge: 2,
  flexMedium: 1.5,
  flexSmall: 1,
  flexTiny: 0.5
}

// brand.forEach((e) => {this[e] = brand[e]})

export const styles = StyleSheet.create({
  container: {
    flex: brand.flexSmall,
    backgroundColor: brand.colorWhite,
    alignItems: 'center',
    justifyContent: 'center',
  },
  titleText: {
    flex: brand.flexMedium,
    fontSize: brand.fontLarge,
    fontWeight: 'bold'
  },
  mainText: {
    flex: brand.flexSmall,
    fontSize: brand.fontMedium,
  },
  listItemComponent: {
    color: brand.colorWhite,
    fontSize: brand.fontMedium,
    fontWeight: '400'
  },
  listItemComponentWrapper: {
    backgroundColor: brand.colorPop,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    width: 200,
    margin: 20,
    flex: brand.flexMedium
  },
  spacer: {
    flex: brand.flexLarge
  },
  inputText: {
    flex: brand.flexTiny, 
    width: 200, 
    justifyContent: 'center',
    alignItems: 'center'
  },
  button: {
    color: brand.colorPop
  }
});
