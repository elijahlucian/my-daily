exampleTask = {
  id: 'uuid',
  name: 'Brush Your Teeth',
  repeated: 'Daily' || 0, // make an enumerable for this. {0: Daily, 1: Weekly, 2: Bi-Weekly, 3: Monthly, 4: Quarterly, 5: Annually}
  hour: 7, // hour of day
  range: 2, // hours to persist task
  alarm: true, // fire push notification if task is not finished during time period
  average: 120 // array of all the time in seconds that the task takes, will be used to calculate average time task takes
}

repeatedEnum = {
  0: "Daily", 1: "Weekly", 3: "Monthly", 6: "Yearly"
}

// give option for times / "repeated" unit