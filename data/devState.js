export const devState = {
  debug: "debug messages go here",
  newTaskDialog: false,
  currentTimer: 0, // this wil be start time in seconds, will be used to measure against the timer stopped.
  taskStartedAt: 0,
  taskTimeElapsed: 0,
  todoListKeys: [],
  selectedTaskId: 0,
  taskInProgress: false,
  todoList: {
    0: { name: "Shit", average: 120000, repeated: 0, hour: 7, range: 1, alarm: false, remind: true, finished: false }, 
    42: { name: "Brush Teeth", average: 120000, repeated: 0, hour: 7, range: 1, alarm: false, remind: true, finished: false }, 
    45: { name: "Shower", average: 1200000, repeated: 0, hour: 7, range: 1, alarm: true, remind: true, finished: false }, 
    955: { name: "Coffee", average: 240000, repeated: 0, hour: 8, range: 0.5, alarm: true, remind: true, finished: false },
    515: { name: "Breakfast", average: 240000, repeated: 0, hour: 8, range: 1, alarm: false, remind: true, finished: false }
  },
  pic: { uri: "https://www.kandeimaging.com/wp-content/uploads/2018/08/1V4A3827.jpg" }
}
