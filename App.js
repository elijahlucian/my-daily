import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity } from 'react-native';

import { styles }  from './data/style';
import { devState }  from './data/devState'

import ListItem from './components/ListItem'
import NewListItem from './components/NewListItem'

export default class App extends Component {
  
  state = devState

  createNewTask = () => {
    this.setState(previousState => {return {newTaskDialog: !previousState.newTaskDialog}})
  }

  newTask = (newTaskName) => {
    taskId = Math.floor( Math.random() * 1000 )
    todoList = this.state.todoList
    todoList[taskId] = { name: newTaskName }
    this.setState({ todoList: todoList, newTaskDialog: false, debug: newTaskName })
    this.updateList()
  }

  clickTask = (key) => {
    // first check if task is in progress
    if (this.state.taskInProgress && key === this.state.selectedTaskId) {
      this.timerStop()
      return
    }
    this.timerStart(key)
    return
  }

  getTimeInSeconds = () => {
    return Math.trunc((new Date).getTime()/1000)
  }

  getCurrentTask = (id = null) => {
    if(id) {
      return this.state.todoList[id]
    }
    return this.state.todoList[this.state.selectedTaskId]
  }

  timerStart = (key) => {
    this.setState({ 
      debug: "starting timer for " + this.getCurrentTask(key).name,
      selectedTaskId: key, 
      taskStartedAt: Date.now(),
      taskInProgress: true 
    })
  }
  
  timerStop = () => {
    let taskTimeElapsed = Date.now() - this.state.taskStartedAt
    let oldAverage = this.getCurrentTask().average
    let newAverage = Math.round((oldAverage + taskTimeElapsed) / 2)
    
    // if currentTimer is deviated over 20%, confirm with the user that this time is correct
    // give option to delete the time
    this.setState({
      debug: (newAverage/1000).toFixed(1) + ":" + " stopping task " + this.getCurrentTask().name,
      taskTimeElapsed: taskTimeElapsed,
      taskInProgress: false
    }) 
    // TODO: change this to just turn task in progress to false
    // then call update task. 
    this.updateTask({key: 'average', value: newAverage}) // 
  }

  updateTask = (body) => {
    // user clicks stop task, then this fires
    // async post
      // axios.post(url, body).
      // then(res => this.setState(res.body))
      // send task finished to the backend
      // upon response, animate currentTask out of the list.
    // paywall - "offline mode"
    newTodoList = this.state.todoList
    newTodoList[this.state.selectedTaskId][body.key] = body.value
    newTodoList[this.state.selectedTaskId]['finished'] = true
    this.setState({ todoList: newTodoList })
  }

  checkSwipeDirection = () => {}

  takePicture = () => {
  }

  handleSwipe = () => {
    // user swipes
    // check direction
    // do something based on direction
    // if right
      // finish task
    // if left
      // snooze task? do some other shit
  }

  checkForNewTasks() {
    // this is fired on a callback timer, every minute, or 15 minutes, or just every hour on the hour
    // filter tasks by time of day
  }

  updateList() {
    // ping firebase for object
      // if database has entries within timeframe, return list
      // update state with todoList Object
      this.setState({ todoListKeys: Object.keys(this.state.todoList) })
  }

  componentDidUpdate() {
    if(this.state.taskInProgress) {
      // if timeElapsed > task.average
      // ding a bell every 5 seconds.
      // requestAnimationFrame()
    }
  }

  componentWillMount() {
    // iteration 1
      // get list of shit to do from local storage
    // iteration 2
      // if user file stored on local, authenticate and load from firebase
      // if no user file found locally, create new user
    // allow every minute / hour tasks? seems overkill... maybe some kind of interval / sprint use... hmm
    this.updateList()
  }

  componentDidMount() { 
  }

  render() {
    let NewTaskModal
      // if user clicks the new task, this modal shows.
      // Text field
      // submit button
      // task time in Minutes:Seconds (click and drag time clock things)
      // 

    let TaskUnderwayModal
      // this will show after teh user has clicked, it will show a persistent timer, and a click anywhere option to stop
      // small cancel button in bottom right - or return arrow
    
    let taskList = this.state.todoListKeys.map((itemKey, i) => {
      return(<ListItem clickTask={this.clickTask} listItem={this.state.todoList[itemKey]} taskKey={itemKey} key={i}/>)
    })

    // newTaskOrList = this.state.newTaskDialog ? <NewListItem newTask={this.newTask} /> : taskList
    let timer = this.state.taskFinishedAt - this.state.taskStartedAt
    let lastTaskTime = timer > 0 ? timer : 0
    
    return (
      <View style={styles.container}>
        <Text style={styles.spacer} />
        <Text style={styles.titleText}>My Daily</Text>
        <Text style={styles.mainText}>{this.getCurrentTask().name}</Text>
        <Text style={styles.mainText}>{this.state.debug}</Text>
        <Text style={styles.mainText}>Timer: {(this.state.taskTimeElapsed/1000).toFixed(1)} seconds</Text>
        {taskList}
        <TouchableOpacity onPress={this.createNewTask}>
          <Image source={require('./data/images/document.png')} style={{width: 75, height: 75}} />
        </TouchableOpacity>
        <Text style={styles.spacer} />
      </View>
    );
  }
}

