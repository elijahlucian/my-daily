import { Text, TouchableOpacity } from 'react-native';
import React, { Component } from 'react';
import { styles }  from '../data/style';

export default class ListItem extends Component {
  
  handleClick = (e, key) => {
    this.props.clickTask(key)
    e.preventDefault()
  }
  
  render() {
    return (
      <TouchableOpacity style={styles.listItemComponentWrapper} onPress={(e) => {this.handleClick(e, this.props.taskKey)}}>
        <Text style={styles.listItemComponent}>{this.props.taskKey}: {this.props.listItem.name} - {(this.props.listItem.average/1000).toFixed(1)}</Text>
      </TouchableOpacity>
    )
  }
}
