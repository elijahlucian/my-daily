import { Text, View, TextInput, Button } from 'react-native';
import React, { Component } from 'react';
import { styles }  from '../data/style';

export default class NewListItem extends Component {
  
  state = {text: ''}
  
  handleClick = (e) => {
    this.props.newTask(this.state.text)
    e.preventDefault()
  }
  handleChange = (event) => {
    this.setState({userInput: event.target.value})
  }
  render() {
    return(
      <View>
      <Text>{this.state.text}</Text>
      <TextInput style={styles.inputText} textAlign={'center'} placeholder="type some shit" onChangeText={(text) => this.setState({text})} />
        <Button
            onPress={this.handleClick}
            title="Add New Task"
            style={styles.button}
        />
      </View>
    )
  }
}
