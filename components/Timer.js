import { Text, View, TextInput, Button } from 'react-native';
import React, { Component } from 'react';
import { styles }  from '../data/style';

export default class ListItem extends Component {
  
  handleClick = (e) => {
    this.props.timerStop()
    e.preventDefault()
  }
  
  render() {
    return (
      <TouchableOpacity style={styles.listItemComponentWrapper} onPress={(e) => {this.handleClick(e)}}>
        <Button
          onPressstop={this.handleClick}
          title="Stop The Timer"
          style={styles.button}
        />
        <Button style={styles.titleText}>stop the timer</Button>
      </TouchableOpacity>
    )
  }
}
